package flight;

import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import data_access.FlightDao;
import entities.City;
import entities.Flight;
import utils.DateUtils;


public class FlightDaoTests extends DBTestCase {
	public FlightDaoTests(String name) {
		super(name);
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, "org.hsqldb.jdbcDriver" );
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, "jdbc:hsqldb:file:D:/airportdb/airport" );
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, "sa" );
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, "" );

        
        flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
	}

	private FlightDao flightDao;
	
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/java/flight/flight_in_1.xml"));
	}

	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.CLEAN_INSERT;
	}

	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}
	
	@Test
	public void testCreate() {

		Flight flight = new Flight();
		City city = new City();
		city.setId(1);
		flight.setNumber(1231);
		flight.setAirplaneType("type");		
		flight.setArrivalCity(city);
		flight.setDepartureCity(city);
		flight.setArrivalDateTime(DateUtils.dateFor(1970, Calendar.JANUARY, 1, 0, 0,0));
		flight.setDepartureDateTime(DateUtils.dateFor(1970, Calendar.JANUARY, 1, 0, 0,0));
		flightDao.addFlight(flight);
		
		// Fetch database data 
        IDataSet databaseDataSet;
		try {
			databaseDataSet = getConnection().createDataSet();
			ITable actualTable = databaseDataSet.getTable("flight");
			
			 // Load expected data from an XML dataset
	        IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new File("src/test/java/flight/flight_out_1.xml"));
	        ITable expectedTable = expectedDataSet.getTable("flight");

	        String[] ignoredCols = {"IDFLIGHT"};
	        Assertion.assertEqualsIgnoreCols(expectedTable, actualTable, ignoredCols);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testUpdate() {

		Flight flight = flightDao.findFlight(1);

		flight.setNumber(1222);
		flight.setAirplaneType("airplane");		
		flight.setDepartureDateTime(new Date(DateUtils.dateFor(2018, Calendar.OCTOBER, 17,22,0,0).getTime()));
		flight.setArrivalDateTime(new Date(DateUtils.dateFor(2018, Calendar.OCTOBER, 18,10,0,0).getTime()));
		flightDao.updateFlight(flight);
		
		// Fetch database data 
        IDataSet databaseDataSet;
		try {
			databaseDataSet = getConnection().createDataSet();
			ITable actualTable = databaseDataSet.getTable("flight");
			 // Load expected data from an XML dataset
	        IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new File("src/test/java/flight/flight_out_update.xml"));
	        ITable expectedTable = expectedDataSet.getTable("flight");

	        // Assert actual database table match expected table
	        Assertion.assertEquals(expectedTable, actualTable);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testFind() {

		Flight flight = flightDao.findFlight(1);
		assertEquals("type",flight.getAirplaneType());
		
		flight.setId(2);
		flightDao.addFlight(flight);
		
		List<Flight> flights = flightDao.findFlights();
		assertEquals(2,flights.size());
	}
	
	@Test
	public void testDelete() {

		Flight flight = flightDao.findFlight(1);
		flightDao.deleteFlight(flight);
		
		List<Flight> flights = flightDao.findFlights();
		assertEquals(0,flights.size());
	}
	
	


}
