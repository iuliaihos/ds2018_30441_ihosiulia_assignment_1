package user;

import java.io.FileInputStream;

import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import data_access.UserDao;
import entities.User;


public class UserDaoTests extends DBTestCase {
	public UserDaoTests(String name) {
		super(name);
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, "org.hsqldb.jdbcDriver" );
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, "jdbc:hsqldb:file:D:/airportdb/airport" );
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, "sa" );
        System.setProperty( PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, "" );

        
        userDao = new UserDao(new Configuration().configure().buildSessionFactory());
	}

	private UserDao userDao;
	
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/java/user/user_in.xml"));
	}

	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.CLEAN_INSERT;
	}

	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}
	

	
	@Test
	public void testFindExistent() {

		User user = userDao.findUserByEmailAndPassword("admin", "admin");
		assertEquals(true, user.getIsAdmin());
		
		user = userDao.findUserByEmailAndPassword("client", "client");
		assertEquals(false, user.getIsAdmin());
	}
	
	@Test
	public void testNotFound() {

		User user = userDao.findUserByEmailAndPassword("admin", "ad");
		assertEquals(null, user);
	}
	

}
