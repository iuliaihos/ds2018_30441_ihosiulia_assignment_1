package utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtils {
    private DateUtils() {
    }

    public static Date dateFor(int year, int month, int day, int hour, int minute, int second) {
        Calendar cal = GregorianCalendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY,hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, 0);
     
        return cal.getTime();
    }
    
    public static Date dateFor(String dateString){
    	
    	DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    	try {
			return format.parse(dateString);
		} catch (ParseException e) {
			return null;
		}
    	 
    }

}