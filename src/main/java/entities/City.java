package entities;

import java.util.Set;

public class City {
	
	private int id;
	private Double latitude;
	private Double longitude;
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}



	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}



	private Set<Flight> flightsArrival;
	private Set<Flight> flightsDeparture;
	
	
	public Set<Flight> getFlightsArrival() {
		return flightsArrival;
	}



	public void setFlightsArrival(Set<Flight> flightsArrival) {
		this.flightsArrival = flightsArrival;
	}



	public Set<Flight> getFlightsDeparture() {
		return flightsDeparture;
	}



	public void setFlightsDeparture(Set<Flight> flightsDeparture) {
		this.flightsDeparture = flightsDeparture;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}





	public Double getLatitude() {
		return latitude;
	}



	public Double getLongitude() {
		return longitude;
	}



	@Override
	public String toString() {
		return "City [id=" + id + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}
	
	
}
