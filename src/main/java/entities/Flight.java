package entities;

import java.util.Date;

/**
 * 
 * @author Ihos Iulia Class describing the flight
 *
 */

public class Flight {

	private int id;
	private int number;
	private String airplaneType;
	private City departureCity;
	private Date departureDateTime;
	private City arrivalCity;
	private Date arrivalDateTime;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public City getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(City departureCity) {
		this.departureCity = departureCity;
	}

	public Date getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(Date departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public City getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(City arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public Date getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(Date arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	@Override
	public String toString() {
		return "Flight[ number = " + this.number + ", airplaneType =  " + this.airplaneType + ", departureCity =  "
				+ this.departureCity + ", departureDateAndTime =  " + this.departureDateTime + ", arrivalCity =  "
				+ this.arrivalCity + ", arrivalDateAndHour =  " + this.arrivalDateTime + " ]";
	}

}
