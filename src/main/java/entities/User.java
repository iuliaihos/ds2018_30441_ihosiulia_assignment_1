package entities;

public class User {
	
	private int id;
	private String email;
	private String password;
	private boolean isAdmin;
	
	
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public boolean getIsAdmin() {
		return isAdmin;
	}



	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}



	@Override
	public String toString() {
		return "User [id=" + id + ", email=" + email + ", password=" + password + ", isAdmin=" + isAdmin + "]";
	}
	
	

}
