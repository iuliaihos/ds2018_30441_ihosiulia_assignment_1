package business_logic;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import data_access.UserDao;
import entities.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDao userDao;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        userDao = new UserDao(new Configuration().configure().buildSessionFactory());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Set response content type
	      response.setContentType("text/html");

	      PrintWriter out = response.getWriter();
	      String title = "Login";
	      String docType =
	         "<!doctype html public \"-//w3c//dtd html 4.0 " +
	         "transitional//en\">\n";
	         
	      out.println(docType +
	         "<html>\n" +
	            "<head><title>" + title + "</title></head>\n" +
	            "<body bgcolor = \"#f0f0f0\">\n" +
	               "<h1 align = \"center\">" + title + "</h1>\n" +
	               "<form action = \"login\" method = \"POST\">"+
	            "Email: <input type = \"text\" name = \"email\">"+
"<br />"+
	            "Password: <input type = \"password\" name = \"password\" /> <br />"+
	            "<input type = \"submit\" value = \"Login\" />"+
	        " </form>"+
	            "</body>"+
	         "</html>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		User user = userDao.findUserByEmailAndPassword(email, password);
		
		if(user!=null){
			Cookie loginCookie = new Cookie("email",email);
			
			//setting cookie to expiry in 30 mins
			loginCookie.setMaxAge(30*60);
			response.addCookie(loginCookie);
			if(user.getIsAdmin()){
				Cookie role = new Cookie("role","ADMIN");
				response.addCookie(role);
				response.sendRedirect("admin.html");
			}
			else{
				Cookie role = new Cookie("role","CLIENT");
				response.addCookie(role);
				response.sendRedirect("client.html");
			}
				
		}else{
			RequestDispatcher dispatcher = request.getRequestDispatcher("/login.html");
		    dispatcher.include(request, response);   
		    
		}
	}

}
