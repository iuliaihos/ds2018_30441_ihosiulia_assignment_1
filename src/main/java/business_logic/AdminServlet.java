package business_logic;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import data_access.FlightDao;
import data_access.UserDao;
import entities.City;
import entities.Flight;
import utils.DateUtils;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/admin")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDao flightDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();

		Cookie[] cookies = request.getCookies();
		for(Cookie cookie: cookies) {
			if(cookie.getName().equals("role"))
				if(!cookie.getValue().equals("ADMIN")) {
					out.println("ACCESS DENIED");
					return;
				}
			if(cookie.getName().equals("email"))
				out.println(cookie.getValue());
		}		
		
	      response.setContentType("text/html");

	      String title = "Admin Page";
	      Flight flight = null;
	      List<String> params = Collections.list(request.getParameterNames());
	      String requestParam;
	      if(params.size()>0)
	    	  requestParam = params.get(0);
	      else
	    	  requestParam = "";
	      
	      String docType =
	         "<!doctype html public \"-//w3c//dtd html 4.0 " +
	         "transitional//en\">\n";
	      
	     
	      if(requestParam.equals("findId")){
	    	  String findId = request.getParameter("findId");
	    	  flight = flightDao.findFlight(Integer.parseInt(findId));
	      }
	    	 
	      out.println(docType +
	         "<html>\n" +
	            "<head><title>" + title + "</title></head>\n" +
	            "<body bgcolor = \"#f0f0f0\">\n" +
	               "<h1 align = \"center\">" + title + "</h1>\n" +
	          
	               "<form action = \"admin\" method = \"GET\">"+
	                  "Find By Id: <input type = \"text\" name = \"findId\"><br />"+
	                  "<input type = \"submit\" value = \"Find\" />"+
	                "</form>"+
	               "<form action = \"admin\" method = \"GET\">"+
	                  "Delete By Id: <input type = \"text\" name = \"deleteId\" /><br />"+
	                  "<input type = \"submit\" value = \"Delete\" />"+
	               "</form>");
	      if(requestParam.equals("deleteId")){
	    	  String deleteId = request.getParameter("deleteId");
	    	  flight = flightDao.findFlight(Integer.parseInt(deleteId));
	    	  flightDao.deleteFlight(flight);
	    	  out.print("Entity deleted");
	    	  }
	      
	      if(requestParam.equals("deleteId")||requestParam.equals("findId")){
	      String id = ""+flight.getId();
	      String type = flight.getAirplaneType();
	      String number = ""+flight.getNumber();
	      String dCity = ""+flight.getDepartureCity();
	      String dTime = flight.getDepartureDateTime().toString();
	      String aCity = ""+flight.getArrivalCity();
	      String aTime = flight.getArrivalDateTime().toString();
	      

	      
	      String foundFlight = 
	    		  "<br><span>"+
	    		  "Id: " + id + "<br />"+
                  "Airplane Type: " + type + "<br />"+
                  "Flight Number:" + number + "<br />"+
                  "Departure City: " + dCity + "<br />"+
                  "Departure Time: " + dTime + "<br />"+
                  "Arrival City: " + aCity + "<br />"+
                  "Arrival Time: " + aTime + "<br /></span><br>";
	      
	      out.println(foundFlight);
	      }
	      String flightForm  =        
	      "<form action = \"admin\" method = \"POST\">"+
	    		      "Id: <input type = \"text\" name = \"id\" value = \"id\"/><br />"+
	                  "Airplane Type: <input type = \"text\" name = \"type\" value = \"type\"/><br />"+
	                  "Flight Number: <input type = \"text\" name = \"number\" value = \"number\"/><br />"+
	                  "Departure City: <input type = \"text\" name = \"dCity\" value = \"dCity\"/><br />"+
	                  "Departure Time: <input type = \"datetime-local\" name = \"dTime\" value = \"dTime\"/><br />"+
	                  "Arrival City: <input type = \"text\" name = \"aCity\" value = \"aCity\"/><br />"+
	                  "Arrival Time: <input type = \"datetime-local\" name = \"aTime\" value = \"atime\"/><br />"+
	                  	"<input type = \"radio\" name = \"op\"  value=\"add\">Add<br>"+
	          			"<input type = \"radio\" name = \"op\" value=\"update\">Update<br>"+
	                  "<input type = \"submit\" value=\"Save\" />"+
	       "</form>";
	      out.println(flightForm);
	         out.println( "</body>"+
	    	         "</html>");
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		   Flight flight = new Flight();  
		  flight.setAirplaneType(request.getParameter("type"));
	      flight.setNumber(Integer.parseInt(request.getParameter("number")));
	      City dCity = new City();
	      dCity.setId(Integer.parseInt(request.getParameter("dCity")));
	      flight.setDepartureCity(dCity);
	      flight.setDepartureDateTime(DateUtils.dateFor(request.getParameter("dTime")));
	      City aCity = new City();
	      aCity.setId(Integer.parseInt(request.getParameter("aCity")));
	      flight.setArrivalCity(aCity);
	      flight.setArrivalDateTime(DateUtils.dateFor(request.getParameter("aTime")));
	      
	   if(request.getParameter("op").equals("add")) {
		   flightDao.addFlight(flight);
	   }
	   else {
		   flight.setId(Integer.parseInt(request.getParameter("id")));
		   flightDao.updateFlight(flight);
	   }
	   
	    RequestDispatcher dispatcher = request.getRequestDispatcher("/admin.html");
	    dispatcher.include(request, response); 
	}

}
