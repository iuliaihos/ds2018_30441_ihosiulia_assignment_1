package business_logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.cfg.Configuration;

import data_access.FlightDao;
import entities.Flight;

/**
 * Servlet implementation class ClientServlet
 */
@WebServlet("/client")
public class ClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightDao flightDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClientServlet() {
        super();
        flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 PrintWriter out = response.getWriter();
		 Cookie[] cookies = request.getCookies();
			for(Cookie cookie: cookies) {
				if(cookie.getName().equals("email"))
					out.println(cookie.getValue());
			}
			
		if(request.getParameterNames().hasMoreElements()) {
			
			String lat = request.getParameter("latitude");
			String lon = request.getParameter("longitude");
			String url = "http://www.earthtools.org/timezone/"+lat+ "/"+lon;
			System.out.println("url : " + url);
			
			URL urlObj = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			StringBuffer resp = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				resp.append(inputLine);
			}
			in.close();

			//print result
			System.out.println(response.toString());
		}
		else {
			response.setContentType("text/html");

			String title = "Client ";
			String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";

			out.println(docType + "<html>\n" + "<head><title>" + title + "</title></head>\n"
					+ "<body bgcolor = \"#f0f0f0\">\n" + "<h1 align = \"center\">" + title + "</h1>\n");
			List<Flight> flights = flightDao.findFlights();
			String flightsList ="<table style=\"width:75%\"><tr>"
						    +"<th>ID</th>"
						    +"<th>Airplane Type</th>"
						    +"<th>Number</th>"
						    +"<th>Departure City</th>"
						    +"<th>Departure Time</th>"
						    +"<th>Arrival City</th>"
						    +"<th>Arrival Time</th>"
			+"</tr>";
			for(Flight flight:flights) {
				flightsList+="<tr>"+
				    "<td>"+ flight.getId() +"</td>"+
				    "<td>"+ flight.getAirplaneType() +"</td>"+
				    "<td>"+ flight.getNumber() +"</td>"+
				    "<td>"+ flight.getDepartureCity() +"</td>"+
				    "<td>"+ flight.getDepartureDateTime() +"</td>"+
				    "<td>"+ flight.getArrivalCity() +"</td>"+
				    "<td>"+ flight.getDepartureDateTime() +"</td>"+
			"</tr>";
				
			}
			flightsList+="</tr>";
			out.println(flightsList);
			String coordinatesForm = "<form action = \"client\" method = \"GET\">" + "Latitude: <input type = \"text\" name = \"latitude\">"
					+ "<br />" + "Longitude: <input type = \"password\" name = \"longitude\" /> <br />"
					+ "<input type = \"submit\" value = \"Get Local time\" />" + " </form>" 
					+ "</body>"
					+ "</html>";
		   out.println(coordinatesForm);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
