package data_access;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entities.Flight;
import entities.User;

public class UserDao {
	private static final Log LOGGER = LogFactory.getLog(UserDao.class);
	
	private SessionFactory sessionFactory;
	
	public UserDao(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	

	@SuppressWarnings("unchecked")
	public User findUserByEmailAndPassword(String email, String password) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE email = :email AND password = :password");
			query.setParameter("email", email);
			query.setParameter("password", password);
			users = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return users != null && !users.isEmpty() ? users.get(0) : null;
	}

}
