package data_access;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entities.Flight;

public class FlightDao {
	private static final Log LOGGER = LogFactory.getLog(FlightDao.class);
	
	private SessionFactory sessionFactory;
	
	public FlightDao(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	public Flight addFlight(Flight flight){
		int flightId = -1;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			flightId = (Integer) session.save(flight);
			flight.setId(flightId);
			tx.commit();
		}catch(HibernateException e){
			if(tx!=null)
				tx.rollback();
			LOGGER.error("",e);
		}finally{
			session.close();
		}
		
		return flight;
	}
	
	public Flight updateFlight(Flight flight){
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			session.update(flight);
			tx.commit();
		}catch(HibernateException e){
			if(tx!=null)
				tx.rollback();
			LOGGER.error("",e);
		}finally{
			session.close();
		}
		
		return flight;
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> findFlights() {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			flights = session.createQuery("FROM Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return flights;
	}

	@SuppressWarnings("unchecked")
	public Flight findFlight(int id) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE id = :id");
			query.setParameter("id", id);
			flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
		if	(flights != null && !flights.isEmpty())
			
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}
	
	public void deleteFlight(Flight flight) {
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
	}
}
